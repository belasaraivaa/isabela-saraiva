//Help source: https://stackoverflow.com/questions/47115482/how-do-you-insert-an-image-into-a-html-golang-file
package main


import ( 
		"time"
		"net/http"
		"fmt"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.163:8092",
			IdleTimeout: duration,
		//	Handler    : 
		}

	http.HandleFunc("/", metodoEUrl)
	http.HandleFunc("/coxinha", coxinha)
	http.Handle("/coxinha/", http.StripPrefix("/coxinha/", http.FileServer(http.Dir("./coxinha"))))

	server.ListenAndServe()
}

func coxinha(w http.ResponseWriter, r *http.Request) {
	
	fmt.Fprintf(w, "<h1>coxinha!</h1>")
    fmt.Fprintf(w, "<img src='coxinha/coxinha.jpg' alt='coxinhas' style='width:1280px;height:720px;'>")
}

func metodoEUrl(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, r.Method + " " + r.URL.Path + "?" + r.URL.RawQuery + r.URL.Fragment)
	return 
}
